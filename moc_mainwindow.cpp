/****************************************************************************
** Meta object code from reading C++ file 'mainwindow.h'
**
** Created: Wed Apr 24 23:14:13 2013
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.1)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "mainwindow.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'mainwindow.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.1. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MainWindow[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      19,   12,   11,   11, 0x0a,
      58,   48,   11,   11, 0x0a,
      99,   93,   11,   11, 0x08,
     139,  133,   11,   11, 0x08,
     168,   11,   11,   11, 0x08,
     194,   11,   11,   11, 0x08,
     220,   11,   11,   11, 0x08,
     246,   11,   11,   11, 0x08,
     272,   11,   11,   11, 0x08,
     298,   11,   11,   11, 0x08,
     324,   11,   11,   11, 0x08,
     350,   11,   11,   11, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_MainWindow[] = {
    "MainWindow\0\0bounds\0createPlaneTexture(double[])\0"
    "start,end\0clipDataRange(vtkIdType,vtkIdType)\0"
    "index\0on_month_slider_valueChanged(int)\0"
    "value\0on_zslider_valueChanged(int)\0"
    "on_color_btn1_8_clicked()\0"
    "on_color_btn1_7_clicked()\0"
    "on_color_btn1_6_clicked()\0"
    "on_color_btn1_5_clicked()\0"
    "on_color_btn1_4_clicked()\0"
    "on_color_btn1_3_clicked()\0"
    "on_color_btn1_2_clicked()\0"
    "on_color_btn1_clicked()\0"
};

void MainWindow::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MainWindow *_t = static_cast<MainWindow *>(_o);
        switch (_id) {
        case 0: _t->createPlaneTexture((*reinterpret_cast< double(*)[]>(_a[1]))); break;
        case 1: _t->clipDataRange((*reinterpret_cast< vtkIdType(*)>(_a[1])),(*reinterpret_cast< vtkIdType(*)>(_a[2]))); break;
        case 2: _t->on_month_slider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->on_zslider_valueChanged((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->on_color_btn1_8_clicked(); break;
        case 5: _t->on_color_btn1_7_clicked(); break;
        case 6: _t->on_color_btn1_6_clicked(); break;
        case 7: _t->on_color_btn1_5_clicked(); break;
        case 8: _t->on_color_btn1_4_clicked(); break;
        case 9: _t->on_color_btn1_3_clicked(); break;
        case 10: _t->on_color_btn1_2_clicked(); break;
        case 11: _t->on_color_btn1_clicked(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MainWindow::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MainWindow::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_MainWindow,
      qt_meta_data_MainWindow, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MainWindow::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MainWindow::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MainWindow::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MainWindow))
        return static_cast<void*>(const_cast< MainWindow*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int MainWindow::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
