#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QtDebug>
#include <QtGui>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_vismode(MainWindow::Sphere)
    , ui(new Ui::MainWindow)
    
    , m_data_mapper(vtkPolyDataMapper::New())
    , m_map_mapper(vtkPolyDataMapper::New())
    , m_data_actor(vtkActor::New())
    , m_map_actor(vtkActor::New())
    
    , m_points(vtkPoints::New())
    , m_xyz_points(vtkPoints::New())
    , m_cellarray(vtkCellArray::New())
    , m_magnitudes(vtkFloatArray::New())
    , m_epoch(vtkLongLongArray::New())
    
    , m_dataset(vtkPolyData::New())
    , m_xyz_dataset(vtkPolyData::New())
    , m_glyph3d(vtkGlyph3D::New())
    
    , m_renderWindow(vtkSmartPointer<vtkRenderWindow>::New())
    , m_renderer(vtkSmartPointer<vtkRenderer>::New())
    
    , m_geofilter(vtkGeometryFilter::New())
    , m_line_glyph(vtkSmartPointer<vtkGlyph3D>::New())
    
    , m_magColorTransfer(vtkSmartPointer<vtkColorTransferFunction>::New())
    
    , m_startdate(QDate::currentDate())
    , m_enddate(QDate(1901, 1, 1))
{
    ui->setupUi(this);
    
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(&m_vtkwidget);
    ui->vtkFrame->setLayout(layout);
    
    m_renderWindow->AddRenderer(m_renderer);
    
    m_vtkwidget.SetRenderWindow(m_renderWindow);
    
    m_magnitudes->SetName("Magnitude");
    m_epoch->SetName("Epoch");
    
    adjustSize();
    resize(800,600);
}



void MainWindow::visualizeData2() {
    m_set_color_transfer_func();
    
    // Add the magnitude labelling
    vtkSmartPointer<vtkScalarBarActor> theScalarBar = vtkSmartPointer<vtkScalarBarActor>::New();
    theScalarBar->SetLookupTable(m_magColorTransfer);
    theScalarBar->SetTitle("Magnitude");
    
    if (m_vismode == MainWindow::Plane) {
        theScalarBar->SetWidth(0.5);
        theScalarBar->SetHeight(0.1);
        theScalarBar->SetPosition(0.25, 0.05);
        theScalarBar->SetOrientationToHorizontal();
        
        //
        // Glyph
        // discards all the cells and draw vertices faster
        //
        vtkSmartPointer<vtkVertexGlyphFilter> glyphfilter = vtkSmartPointer<vtkVertexGlyphFilter>::New();
        glyphfilter->SetInputConnection(m_dataset->GetProducerPort());
        glyphfilter->Update();
        
        //
        // point clipping
        // with the geometryFilter to filter geometry (points) by date
        //
        m_geofilter->SetInputConnection(glyphfilter->GetOutputPort());
        m_geofilter->PointClippingOn();
        
        m_data_mapper->SetColorModeToMapScalars();
        m_data_mapper->SetInputConnection(m_geofilter->GetOutputPort());
        m_data_actor->GetProperty()->SetBackfaceCulling(1);
    }
    
    else {
        theScalarBar->SetWidth(0.05);
        theScalarBar->SetHeight(0.8);
        theScalarBar->SetPosition(0.05, 0.1);
        theScalarBar->SetOrientationToVertical();
        
        vtkSmartPointer<vtkLineSource> line = vtkSmartPointer<vtkLineSource>::New();
        m_line_glyph->SetSourceConnection(line->GetOutputPort());
        m_line_glyph->SetInputConnection(m_xyz_dataset->GetProducerPort());
        m_line_glyph->SetScaleModeToScaleByVector();
//        m_line_glyph->SetScaleModeToScaleByScalar();
        m_line_glyph->SetColorModeToColorByScalar();
        m_line_glyph->SetScaleFactor(2);
        
        m_data_mapper->SetInputConnection(m_line_glyph->GetOutputPort());
    }
    
    m_data_mapper->SetLookupTable(m_magColorTransfer);
    
    m_data_actor->SetMapper(m_data_mapper);
    
    m_renderer->AddActor(m_data_actor);
    m_renderer->AddActor(theScalarBar);
    m_renderer->ResetCamera();
}

void MainWindow::clipDataRange(vtkIdType start, vtkIdType end) {
    if (end > m_epoch->GetMaxId()) return;
    qDebug() << __FUNCTION__ << start << end;
    m_geofilter->SetPointMinimum(start);
    m_geofilter->SetPointMaximum(end);
    m_geofilter->Modified();
}


MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_month_slider_valueChanged(int index)
{
    if (index >= m_date_vtkid.size()-1) return;
    
    if (index < 0) return;
    
    int i = 0;
    QMap<QDate, vtkIdType>::iterator it = m_date_vtkid.begin();
    for (; (it+1) != m_date_vtkid.end(); it++) {
        if (i == index) {
            clipDataRange( it.value(), (it+1).value() );
            ui->monthslider_label->setText( it.key().toString() + " ~ " + (it+1).key().toString() );
            break;
        }
        ++i;
    }
}

void MainWindow::on_zslider_valueChanged(int value)
{
    if (!m_data_actor  ||  !m_renderer)  return;
    
    double scale = double(value) / 10;
    
    if (m_vismode == MainWindow::Plane) {
        m_data_actor->SetScale(1, 1, scale);
        m_data_actor->Modified();
        m_renderer->Render();
    }
    else {
        int input = m_line_glyph->GetTotalNumberOfInputConnections();
        if (input < 1) return;
        // change scale of the glyph with the line source
        m_line_glyph->SetScaleFactor(scale);
        m_line_glyph->Update();
        m_line_glyph->Modified();
    }
    m_renderer->Render();
}



void MainWindow::createPlaneTexture(double bounds[3]) {
    //
    // Bathymetry
    //
    vtkSmartPointer<vtkJPEGReader> theReader = vtkSmartPointer<vtkJPEGReader>::New();
    theReader->SetFileName("/Users/snam5/Dev/earthquake_vis/Data/bathymetry2.jpg");

    vtkSmartPointer<vtkImageShrink3D> bathy_subsample = vtkSmartPointer<vtkImageShrink3D>::New( );
    bathy_subsample->SetShrinkFactors( 8, 8, 1 );
    bathy_subsample->SetInput( theReader->GetOutput() );

    // extract geometry form a structured points dataset
    vtkSmartPointer<vtkImageDataGeometryFilter> theGeometry = vtkSmartPointer<vtkImageDataGeometryFilter>::New();
    theGeometry->SetInputConnection(bathy_subsample->GetOutputPort());
    theGeometry->Update();

    // generates triangles
    vtkSmartPointer<vtkTriangleFilter> theTriangleFilter = vtkSmartPointer<vtkTriangleFilter>::New();
    theTriangleFilter->SetInput(theGeometry->GetOutput());

    // a filter that modifies point coordinates by 
    // moving points along point normals by the scalar amount times the scale factor. 
    vtkSmartPointer<vtkWarpScalar> theWarpScalar = vtkSmartPointer<vtkWarpScalar>::New();
    theWarpScalar->SetInput(theTriangleFilter->GetOutput());
    //theWarpScalar->SetScaleFactor(1);
    //theWarpScalar->UseNormalOn();
    //theWarpScalar->SetNormal(0, 0, 1);
    //theWarpScalar->GetPolyDataOutput()->GetPointData()->SetTCoords(textureCoords);
    theWarpScalar->Update();

    // reduce triangles using vtkDecimatePro
    vtkSmartPointer<vtkDecimatePro> reduction = vtkSmartPointer<vtkDecimatePro>::New();
    reduction->SetInput(theWarpScalar->GetPolyDataOutput());
    reduction->SetPreserveTopology(1);
    reduction->SetTargetReduction(0.5); //70% less triangles

    vtkSmartPointer<vtkPolyDataNormals> normals = vtkSmartPointer<vtkPolyDataNormals>::New();
    normals->SetInput(reduction->GetOutput());
    //normals->SetFeatureAngle(60);
    normals->SplittingOff();

    
    
    
    
    //
    // texture
    //
    vtkSmartPointer<vtkJPEGReader> theReader2 = vtkSmartPointer<vtkJPEGReader>::New();
    theReader2->SetFileName("/Users/snam5/Dev/earthquake_vis/Data/2_no_clouds_8k.jpg");

    vtkSmartPointer<vtkImageShrink3D> theReader2Shrinker = vtkSmartPointer<vtkImageShrink3D>::New( );
    theReader2Shrinker->SetShrinkFactors( 8, 8, 1 );
    theReader2Shrinker->SetInput( theReader2->GetOutput() );

    vtkSmartPointer<vtkTexture> surfaceTexture = vtkSmartPointer<vtkTexture>::New();
    surfaceTexture->SetInput(theReader2Shrinker->GetOutput());
    //surfaceTexture->SetInterpolate(true);
    //surfaceTexture->SetRepeat(0);
    surfaceTexture->RepeatOff();

    
    vtkSmartPointer<vtkLookupTable> lut = vtkSmartPointer<vtkLookupTable>::New();
    lut->Build();

    vtkSmartPointer<vtkMergeFilter> merger = vtkSmartPointer<vtkMergeFilter>::New( );
    vtkSmartPointer<vtkGeometryFilter> geomFilter2 = vtkSmartPointer<vtkGeometryFilter>::New( );
    vtkSmartPointer<vtkTextureMapToPlane> theTPlane = vtkSmartPointer<vtkTextureMapToPlane>::New();
    vtkSmartPointer<vtkTransformTextureCoords> theCoords = vtkSmartPointer<vtkTransformTextureCoords>::New();
    
    merger->SetGeometry( ( vtkDataSet * ) ( reduction->GetOutput( ) ) );
    merger->SetScalars( ( vtkDataSet * ) ( theReader2Shrinker->GetOutput( ) ) );
    geomFilter2->SetInput( merger->GetOutput( ) );
    theTPlane->SetInputConnection(geomFilter2->GetOutputPort());
    theCoords->SetInput(theTPlane->GetOutput());
    
    
    m_map_mapper->SetInputConnection(theCoords->GetOutputPort());
    m_map_mapper->SetLookupTable(lut);
    
    m_map_actor->SetMapper(m_map_mapper);
    m_map_actor->GetProperty()->SetOpacity(0.7);
    m_map_actor->GetProperty()->SetBackfaceCulling(1);
    m_map_actor->SetTexture(surfaceTexture);
    m_map_actor->SetScale(1.0, 1.0, 5.0);
    
    m_renderer->AddActor(m_map_actor);
    
    
    
    
    // To assign the coordinates, get the bounding box of the bathymetry
    // vtkDataSet::GetBounds() returns array of (xmin, xmax, ymin, ymax, zmin, zmax)
    double *imageBounds = theWarpScalar->GetOutput()->GetBounds();
//    qDebug() << "warpscalar bounds " << imageBounds[0] << imageBounds[1] << imageBounds[2] << imageBounds[3] << imageBounds[4] << imageBounds[5];
    // xmax, ymax, zmax
    bounds[0] = imageBounds[1]; // x max
    bounds[1] = imageBounds[3]; // y max
    bounds[2] = imageBounds[5]; // z max
}

void MainWindow::createSphereTexture(const char *jpegfile) {
    vtkSmartPointer<vtkJPEGReader> jpeg = vtkSmartPointer<vtkJPEGReader>::New();
    jpeg->SetFileName(jpegfile);

    vtkSmartPointer<vtkImageShrink3D> imgShrinker = vtkSmartPointer<vtkImageShrink3D>::New();
    imgShrinker->SetShrinkFactors( 8, 8, 1 );
    imgShrinker->SetInput( jpeg->GetOutput() );

    vtkSmartPointer<vtkTexture> surfaceTexture = vtkSmartPointer<vtkTexture>::New();
    surfaceTexture->SetInput(imgShrinker->GetOutput());
    //surfaceTexture->SetInterpolate(true);
    //surfaceTexture->SetRepeat(0);
    surfaceTexture->RepeatOff();
    
    vtkSphereSource *sphere = vtkSphereSource::New();
    sphere->SetRadius(Earth_Radius);
    sphere->SetCenter(0,0,0);
    sphere->SetThetaResolution(512);
    sphere->SetPhiResolution(512);
    
    vtkTextureMapToSphere *textureToSphere = vtkTextureMapToSphere::New();
    textureToSphere->SetInput(sphere->GetOutput());
    textureToSphere->PreventSeamOff();
    
    vtkPolyDataMapper *spheremapper = vtkPolyDataMapper::New();
    spheremapper->SetInput( vtkPolyData::SafeDownCast( textureToSphere->GetOutput()));
    
    vtkActor *sphereactor = vtkActor::New();
    sphereactor->SetMapper(spheremapper);
    sphereactor->SetTexture(surfaceTexture);
    sphereactor->GetProperty()->SetOpacity(1);
    sphereactor->RotateX(90); // to align with the data
    
    m_renderer->AddActor(sphereactor);
    m_renderer->ResetCamera();
}

/*!
 * \brief MainWindow::createEarthquakeData This is what victor did
 * \param filename The .csv data file (sorted by date)
 * \param xbound Max x of the texture image (right most)
 * \param ybound Max y of the texture image (top most)
 * \param zbound Max z of the texture image (elevation)
 */
void MainWindow::createEarthquakeData(const char* filename, double xbound, double ybound, double zbound) {
    vtkSmartPointer<vtkDelimitedTextReader> textReader = vtkSmartPointer<vtkDelimitedTextReader>::New();
    textReader->SetFileName(filename);
    textReader->SetHaveHeaders(true);
    textReader->SetDetectNumericColumns(true);
    textReader->SetFieldDelimiterCharacters(",");
    textReader->Update();
    
    vtkTable* table = textReader->GetOutput();

    cout << "Number of Rows: " << table->GetNumberOfRows() << endl;
    cout << "Number of Columns: " << table->GetNumberOfColumns() << endl;
    /*
     * Year,Month,Day,Time(hhmmss.mm)UTC,Latitude,Longitude,Magnitude,Depth,Catalog
     */
    
    vtkDoubleArray* longitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Longitude")); // x
    vtkDoubleArray* latitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Latitude")); // y
    vtkIntArray* depth = vtkIntArray::SafeDownCast(table->GetColumnByName("Depth")); // z, Km
    vtkDoubleArray* magnitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Magnitude")); // scalar data
    
    vtkIntArray* year = vtkIntArray::SafeDownCast(table->GetColumnByName("Year"));
    vtkIntArray* month = vtkIntArray::SafeDownCast(table->GetColumnByName("Month"));
    vtkIntArray* day = vtkIntArray::SafeDownCast(table->GetColumnByName("Day"));
    vtkDoubleArray* time = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Time(hhmmss.mm)UTC")); 
    
    for (int i=0; i<table->GetNumberOfRows(); i++) {
        // lat -90, lon -180  == (0,0,z) is bottom left
        m_points->InsertPoint(
                    i,
                    xbound * (longitude->GetValue(i) + 180.0) / 359.0,
                    ybound * (latitude->GetValue(i) + 90.0) / 179.0,
                    zbound * depth->GetValue(i) * -1 /*000.0/8848.0*/);
//        qDebug() << zbound * (double)depth->GetValue(i) * -1000.0/8848.0;

/*        
        vtkVertex *cell = vtkVertex::New();
        cell->GetPoints()->InsertPoint(i, m_points->GetPoint(i));
        m_cellarray->InsertNextCell(cell);
        cell->Delete();
  */      
        m_create_scalar(i, magnitude->GetValue(i), QDate(year->GetValue(i),month->GetValue(i),day->GetValue(i)), time->GetValue(i));
    }
    
    m_dataset->SetPoints(m_points); // geometry (point)
//    m_dataset->SetVerts(m_cellarray); // topology (cell)
    m_dataset->GetPointData()->SetScalars(m_magnitudes);
//    m_dataset->GetPointData()->AddArray(m_epoch);
    
    /*
    double *range = m_dataset->GetPointData()->GetArray("Magnitude")->GetRange();
    qDebug() << "Magnitude range " << range[0] << range[1];
    double *drange = m_dataset->GetPointData()->GetArray("Epoch")->GetRange();
    qDebug() << "Epoch range " << drange[0] << drange[1];
    qDebug() << "# Epoch data " << m_dataset->GetPointData()->GetArray("Epoch")->GetDataSize();
    */
}

/*!
 * \brief MainWindow::createEarthquakeData converts the data point (latitude,longitude) to XYZ to visualize them on a sphere
 * \param filename
 */
void MainWindow::createEarthquakeData(const char *filename) {
    vtkSmartPointer<vtkDelimitedTextReader> textReader = vtkSmartPointer<vtkDelimitedTextReader>::New();
    textReader->SetFileName(filename);
    textReader->SetHaveHeaders(true);
    textReader->SetDetectNumericColumns(true);
    textReader->SetFieldDelimiterCharacters(",");
    textReader->Update();
    
    vtkTable* table = textReader->GetOutput();

    cout << "Number of Rows: " << table->GetNumberOfRows() << endl;
    cout << "Number of Columns: " << table->GetNumberOfColumns() << endl;
    /*
     * Year,Month,Day,Time(hhmmss.mm)UTC,Latitude,Longitude,Magnitude,Depth,Catalog
     */
    
    vtkDoubleArray* longitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Longitude")); // x
    vtkDoubleArray* latitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Latitude")); // y
    vtkIntArray* depth = vtkIntArray::SafeDownCast(table->GetColumnByName("Depth")); // z, Km
    vtkDoubleArray* magnitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Magnitude")); // scalar data
    
    vtkIntArray* year = vtkIntArray::SafeDownCast(table->GetColumnByName("Year"));
    vtkIntArray* month = vtkIntArray::SafeDownCast(table->GetColumnByName("Month"));
    vtkIntArray* day = vtkIntArray::SafeDownCast(table->GetColumnByName("Day"));
    vtkDoubleArray* time = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Time(hhmmss.mm)UTC")); 
    
    QDateTime datetime;
    datetime.setTimeSpec(Qt::UTC);
    
    vtkFloatArray* vectorarray = vtkFloatArray::New();
    
    for (quint32 i=0; i<table->GetNumberOfRows(); i++) {
        double mag = magnitude->GetValue(i);
        
        // lat, lon to XYZ
        double lat = latitude->GetValue(i) * M_PI / 180.0; // to radian
        double lon = longitude->GetValue(i) * M_PI / 180.0; // to radian
        vtkIdType pt1 = m_xyz_points->InsertNextPoint(
                                  -1.0 * Earth_Radius * cos(lat) * cos(lon),
                                  Earth_Radius * sin(lat),
                                  //(Earth_Radius - depth->GetValue(i)) * cos(lat) * sin(lon) // points inside the globe
                                  Earth_Radius * cos(lat) * sin(lon) // point on the globe surface => no depth info in the coordinate !!
                                  );
        
        m_magnitudes->InsertNextValue(mag);
        
        // vertex cell
        vtkSmartPointer<vtkVertex> vertexcell = vtkSmartPointer<vtkVertex>::New();
        vertexcell->GetPointIds()->InsertNextId(pt1);
        
        m_cellarray->InsertNextCell(vertexcell);
        
        // vector of the scalar data
        vectorarray->SetNumberOfComponents(3);
        vectorarray->InsertNextTuple3(
                                  mag * -100 * cos(lat) * cos(lon),
                                  mag * 100 * sin(lat),
                                  mag * 100 * cos(lat) * sin(lon)
                                  );
        
        
        datetime.setDate(QDate(year->GetValue(i), month->GetValue(i), day->GetValue(i)));
        
        // 213558.60   == 21:35:58.60
        double utctime = time->GetValue(i);
        int hh = utctime / 10000.0;
        int mm = ((int)utctime / 100) % 100;
        int ss = (int)utctime % 100;
        datetime.setTime(QTime(hh, mm, ss));
        
        m_epoch->InsertValue(i, datetime.toMSecsSinceEpoch());
        
        // find start/end date of data
        if (datetime.date() < m_startdate) m_startdate = datetime.date();
        if (datetime.date() > m_enddate) m_enddate = datetime.date();
        
        // keep the data point at the 1st day of every month in the map container
        if (day->GetValue(i) == 1) {
            QMap<QDate, vtkIdType>::const_iterator it = m_date_vtkid.find(datetime.date());
            if (it == m_date_vtkid.constEnd()) {
                it = m_date_vtkid.insert(datetime.date(), i); // this assumes the raw data.csv is sorted by date
            }
        }
    }
    
    m_xyz_dataset->SetPoints(m_xyz_points); // geometry
//    m_xyz_dataset->SetLines(m_cellarray);   // line topology
    m_xyz_dataset->SetVerts(m_cellarray); // vertiex topology
    m_xyz_dataset->GetPointData()->SetScalars(m_magnitudes);
    m_xyz_dataset->GetPointData()->SetVectors(vectorarray);
    vectorarray->Delete();
    
//    m_create_vtk_file("/Users/snam5/Dev/earthquake_vis/Data/data.vtk", m_xyz_dataset);
}

void MainWindow::init_gui() {
    /*
    m_magColor[0].setRgb(0,  255, 0);
    m_magColor[1].setRgb(36, 218, 0);
    m_magColor[2].setRgb(63, 181, 0);
    m_magColor[3].setRgb(99, 135, 0);
    m_magColor[4].setRgb(135, 99, 0);
    m_magColor[5].setRgb(181, 63, 0);
    m_magColor[6].setRgb(218, 36, 0);
    m_magColor[7].setRgb(255,  0, 0);
    */
    m_magColor[0].setRgb(251, 233, 233);
    m_magColor[1].setRgb(252, 200, 200);
    m_magColor[2].setRgb(254, 183, 190);
    m_magColor[3].setRgb(244, 150, 150);
    m_magColor[4].setRgb(218, 90, 90);
    m_magColor[5].setRgb(168, 45, 45);
    m_magColor[6].setRgb(158, 25, 25);
    m_magColor[7].setRgb(124,  12, 0);
    
    QPixmap pm(ui->color_lbl1->size());
    pm.fill(m_magColor[0]); ui->color_lbl1->setPixmap(pm);
    pm.fill(m_magColor[1]); ui->color_lbl1_2->setPixmap(pm);
    pm.fill(m_magColor[2]); ui->color_lbl1_3->setPixmap(pm);
    pm.fill(m_magColor[3]); ui->color_lbl1_4->setPixmap(pm);
    pm.fill(m_magColor[4]); ui->color_lbl1_5->setPixmap(pm);
    pm.fill(m_magColor[5]); ui->color_lbl1_6->setPixmap(pm);
    pm.fill(m_magColor[6]); ui->color_lbl1_7->setPixmap(pm);
    pm.fill(m_magColor[7]); ui->color_lbl1_8->setPixmap(pm);

    // init the date slider    
    ui->month_slider->setMinimum(0);
    ui->month_slider->setMaximum(m_date_vtkid.size()-2);
    ui->month_slider->setValue(0);
    
    ui->zslider->setRange(1, 100); // depth scaler
}

vtkIdType MainWindow::m_read_earthquake_date(const char *filename, 
                                        vtkDoubleArray **longitude, vtkDoubleArray **latitude, vtkIntArray **depth, vtkDoubleArray **magnitude,
                                        vtkIntArray **year, vtkIntArray **month, vtkIntArray **day, vtkDoubleArray **time
                                        ) 
{
    vtkSmartPointer<vtkDelimitedTextReader> textReader = vtkSmartPointer<vtkDelimitedTextReader>::New();
    textReader->SetFileName(filename);
    textReader->SetHaveHeaders(true);
    textReader->SetDetectNumericColumns(true);
    textReader->SetFieldDelimiterCharacters(",");
    textReader->Update();
    
    vtkTable* table = textReader->GetOutput();

    cout << "Number of Rows: " << table->GetNumberOfRows() << endl;
    cout << "Number of Columns: " << table->GetNumberOfColumns() << endl;
    /*
     * Year,Month,Day,Time(hhmmss.mm)UTC,Latitude,Longitude,Magnitude,Depth,Catalog
     */
    
    *longitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Longitude")); // x
    *latitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Latitude")); // y
    *depth = vtkIntArray::SafeDownCast(table->GetColumnByName("Depth")); // z, Km
    *magnitude = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Magnitude")); // scalar data
    
    *year = vtkIntArray::SafeDownCast(table->GetColumnByName("Year"));
    *month = vtkIntArray::SafeDownCast(table->GetColumnByName("Month"));
    *day = vtkIntArray::SafeDownCast(table->GetColumnByName("Day"));
    *time = vtkDoubleArray::SafeDownCast(table->GetColumnByName("Time(hhmmss.mm)UTC")); 
    
    return table->GetNumberOfRows();
}

void MainWindow::m_create_scalar(vtkIdType id, double magnitude, const QDate &date, double utctime) {
    
    m_magnitudes->InsertValue(id, magnitude);
    
    QDateTime datetime;
    datetime.setTimeSpec(Qt::UTC);
    datetime.setDate(date);
    
    // 213558.60   == 21:35:58.60
    int hh = utctime / 10000.0;
    int mm = ((int)utctime / 100) % 100;
    int ss = (int)utctime % 100;
    datetime.setTime(QTime(hh, mm, ss));
    
    m_epoch->InsertValue(id, datetime.toMSecsSinceEpoch());
    
    // find start/end date of data
    if (datetime.date() < m_startdate) m_startdate = datetime.date();
    if (datetime.date() > m_enddate) m_enddate = datetime.date();
    
    // keep the data point at the 1st day of every month in the map container
    if (date.day() == 1) {
        QMap<QDate, vtkIdType>::const_iterator it = m_date_vtkid.find(datetime.date());
        if (it == m_date_vtkid.constEnd()) {
            it = m_date_vtkid.insert(datetime.date(), id); // this assumes the raw data.csv is sorted by date
        }
    }
}

void MainWindow::m_set_color_transfer_func() {
    for( int i = 0; i < 8; i++ ) {
        m_magColorTransfer->AddRGBPoint(i + 1.0, (float) m_magColor[i].red() / 255.0, (float) m_magColor[i].green() / 255.0, (float) m_magColor[i].blue() / 255.0);
        m_magColorTransfer->AddRGBPoint(i + 1.9, (float) m_magColor[i].red() / 255.0, (float) m_magColor[i].green() / 255.0, (float) m_magColor[i].blue() / 255.0);
    }
    m_magColorTransfer->AddRGBPoint(9.0, (float) m_magColor[7].red() / 255.0, (float) m_magColor[7].green() / 255.0, (float) m_magColor[7].blue() / 255.0);
    m_magColorTransfer->SetScaleToLinear();
    m_magColorTransfer->Modified();
}

void MainWindow::m_create_vtk_file(const char *filename, vtkDataSet *dataset) {
    vtkPolyDataWriter *writer = vtkPolyDataWriter::New();
    writer->SetFileName(filename);
    writer->SetFileTypeToASCII();
    writer->SetInput(dataset);
    writer->Write();
    writer->Delete();
}

void MainWindow::on_color_btn1_8_clicked() {
    m_magColor[7] = QColorDialog::getColor( m_magColor[7] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[7]);
    ui->color_lbl1_8->setPixmap(pm);
}
void MainWindow::on_color_btn1_7_clicked() {
    m_magColor[6] = QColorDialog::getColor( m_magColor[6] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[6]);
    ui->color_lbl1_7->setPixmap(pm);
}
void MainWindow::on_color_btn1_6_clicked() {
    m_magColor[5] = QColorDialog::getColor( m_magColor[5] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[5]);
    ui->color_lbl1_6->setPixmap(pm);
}
void MainWindow::on_color_btn1_5_clicked() {
    m_magColor[4] = QColorDialog::getColor( m_magColor[4] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[4]);
    ui->color_lbl1_5->setPixmap(pm);
}
void MainWindow::on_color_btn1_4_clicked() {
    m_magColor[3] = QColorDialog::getColor( m_magColor[3] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[3]);
    ui->color_lbl1_4->setPixmap(pm);
}
void MainWindow::on_color_btn1_3_clicked() {
    m_magColor[2] = QColorDialog::getColor( m_magColor[2] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[2]);
    ui->color_lbl1_3->setPixmap(pm);
}
void MainWindow::on_color_btn1_2_clicked() {
    m_magColor[1] = QColorDialog::getColor( m_magColor[1] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[1]);
    ui->color_lbl1_2->setPixmap(pm);
}
void MainWindow::on_color_btn1_clicked() {
    m_magColor[0] = QColorDialog::getColor( m_magColor[0] , this);
    m_set_color_transfer_func();
    QPixmap pm;
    pm.fill(m_magColor[0]);
    ui->color_lbl1->setPixmap(pm);
}
