#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtCore>
#include <QMainWindow>
#include <QColorDialog>

#include <QVTKWidget.h>
#include <vtkVertex.h>
#include <vtkColorTransferFunction.h>
#include <vtkVertexGlyphFilter.h>
#include <vtkWarpVector.h>
#include <vtkDoubleArray.h>
#include <vtkSmartPointer.h>
#include <vtkPolyDataMapper.h>
#include <vtkActor.h>
#include <vtkImageViewer.h>
#include <vtkRenderWindowInteractor.h>
//#include <vtkInteractorStyleImage.h>
#include <vtkRenderer.h>
#include <vtkDataSetWriter.h>
//#include <vtkSphereSource.h>
#include <vtkTubeFilter.h>
#include <vtkUnsignedIntArray.h>

#include <vtkPolyData.h>
#include <vtkPoints.h>
#include <vtkPointData.h>
#include <vtkCellArray.h>
#include <vtkFloatArray.h>
#include <vtkLongLongArray.h>

#include <vtkJPEGReader.h>
#include <vtkImageMapper3D.h>
#include <vtkActor2D.h>

#include <vtkImageData.h>
#include <vtkDataWriter.h>
#include <vtkPolyDataWriter.h>
#include <vtkDataSetMapper.h>
#include <vtkLine.h>
#include <vtkGlyph3D.h>
#include <vtkLineSource.h>
#include <vtkSphereSource.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkTransform.h>
#include <vtkCellData.h>

#include <vtkPolyDataAlgorithm.h>
#include <vtkClipPolyData.h>
#include <vtkImplicitWindowFunction.h>

#include <vtkGeometryFilter.h>
#include <vtkTransform.h>
#include <vtkLinearTransform.h>

#include <vtkContourWidget.h>
#include <vtkOrientedGlyphContourRepresentation.h>
#include <vtkProperty.h>
#include <vtkProjectedTerrainPath.h>
#include <vtkImageDataGeometryFilter.h>
#include <vtkTransformTextureCoords.h>
#include <vtkTextureMapToPlane.h>
#include <vtkPointData.h>
#include <vtkGeoAssignCoordinates.h>
#include <vtkPointSource.h>
#include <vtkUnstructuredGrid.h>
#include <vtkDataSetSurfaceFilter.h>
#include <vtkGlyph3D.h>
#include <vtkSphereSource.h>
#include <vtkGeometryFilter.h>
#include <vtkTransform.h>
#include <vtkRendererCollection.h>
#include <vtkShrinkPolyData.h>
#include <vtkQuadricDecimation.h>
#include <vtkSmoothPolyDataFilter.h>
#include <vtkAxes.h>
#include <vtkDecimatePro.h>
#include <vtkTensorGlyph.h>
#include <vtkImageMapToColors.h>
#include <vtkOutlineFilter.h>
#include <vtkCompassRepresentation.h>
#include <vtkMergeFilter.h>
#include <vtkOpenGLPolyDataMapper.h>
#include <vtkAppendFilter.h>
#include <vtkImageMask.h>
#include <vtkTubeFilter.h>
#include <vtkCubeSource.h>

#include <vtkCompassWidget.h>
#include <vtkInteractorStyleTrackballCamera.h>
#include <vtkUnsignedIntArray.h>
#include <vtkImageShrink3D.h>
#include <vtkPNGReader.h>
#include <vtkDelaunay3D.h>

#include <vtkSurfaceReconstructionFilter.h>
#include <vtkContourFilter.h>
#include <vtkDataSetMapper.h>

#include <vtkSphericalTransform.h>
#include <vtkTransformPolyDataFilter.h>
#include <vtkProperty.h>
#include <vtkTriangleFilter.h>
#include <vtkWarpScalar.h>
#include <vtkPolyDataNormals.h>
#include <vtkLookupTable.h>
#include <vtkDelimitedTextReader.h>
#include <vtkTable.h>
#include <vtkEarthSource.h>
#include <vtkTextureMapToSphere.h>
#include <vtkGlyphSource2D.h>
#include <vtkGlyph2D.h>
#include <vtkScalarBarActor.h>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
    
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
    void setEarthquakeData(int year, int month, int day, qreal lat, qreal lon, qreal depth, qreal mag);
    
   
    enum VisMode {Plane, Sphere};
    
    void createSphereTexture(const char * jpegfile);
    
    void createEarthquakeData(const char *filename, double xbound, double ybound, double zbound);
    
    void createEarthquakeData(const char *filename);
    
    void init_gui();
    
    void visualizeData2();
    
    inline void setVisMode(MainWindow::VisMode m) {m_vismode = m;}
    
private:
    const static double Earth_Radius = 6371.0; /*!< earth's radius in Km */
    
    Ui::MainWindow *ui;
    
    VisMode m_vismode;
    
    QVTKWidget m_vtkwidget;
    
    /*!
     * \brief m_mapper earthquake data vis
     */
    vtkPolyDataMapper *m_data_mapper;
    
    /*!
     * \brief m_map_mapper bathymetry and texture
     */
    vtkPolyDataMapper *m_map_mapper;
    
    vtkActor *m_data_actor;
    vtkActor *m_map_actor;
    
    /*!
     * \brief m_pointIds holds the indice of the geometry of the dataset
     */
    QMap<QDate, vtkIdType> m_date_vtkid;
    
    /*!
     * \brief m_points is the geometry of the dataset
     */
    vtkPoints *m_points;
    
    vtkPoints *m_xyz_points;
    
    /*!
     * \brief m_cellarray is a topology of the dataset
     */
    vtkCellArray *m_cellarray;
    
    /*!
     * \brief m_magnitudes is an array of earthquake magnitude scalar
     */
    vtkFloatArray *m_magnitudes;
    
    vtkLongLongArray *m_epoch;
    
    /*!
     * \brief m_dataset The dataset
     */
    vtkPolyData *m_dataset;
    
    vtkPolyData *m_xyz_dataset;
    
    vtkGlyph3D *m_glyph3d;
    
    vtkSmartPointer<vtkRenderWindow> m_renderWindow;
    vtkSmartPointer<vtkRenderer> m_renderer;
    
    /*!
     * \brief m_geofilter A geometryFilter to clip points based on dates
     */
    vtkGeometryFilter *m_geofilter;
    
    /*!
     * \brief m_warpVector move points based on their vector
     *
     * This is to use with sphere visualization.
     * Each point data has a vector. Use this vector to move points and create line visualization
     */
    vtkSmartPointer<vtkGlyph3D> m_line_glyph;
    
    QColor m_magColor[8];
    
    vtkSmartPointer<vtkColorTransferFunction> m_magColorTransfer;
    
    QDate m_startdate; /*!< data start date */
    QDate m_enddate; /*!< data end date */
    
    vtkIdType m_read_earthquake_date(const char *filename, vtkDoubleArray **longitude, vtkDoubleArray **latitude, vtkIntArray **depth, vtkDoubleArray **magnitude, vtkIntArray **year, vtkIntArray **month, vtkIntArray **day, vtkDoubleArray **time);
    
    void m_create_scalar(vtkIdType id, double magnitude, const QDate &date, double utctime);
    
    void m_create_vtk_file(const char *filename, vtkDataSet *dataset);
    
    void m_set_color_transfer_func();
    
public slots:
    void createPlaneTexture(double bounds[]);
    
    void clipDataRange(vtkIdType start, vtkIdType end);
    
private slots:
    void on_month_slider_valueChanged(int index);
    void on_zslider_valueChanged(int value);
    
    void on_color_btn1_8_clicked(); 
     void on_color_btn1_7_clicked() ;
     void on_color_btn1_6_clicked() ;
     void on_color_btn1_5_clicked() ;
     void on_color_btn1_4_clicked();
     void on_color_btn1_3_clicked();
     void on_color_btn1_2_clicked();
     void on_color_btn1_clicked();
};

#endif // MAINWINDOW_H
