#-------------------------------------------------
#
# Project created by QtCreator 2013-04-20T19:26:26
#
#-------------------------------------------------

QT       += core gui

TARGET = earthquake

CONFIG   += app_bundle

TEMPLATE = app

VTKHOME = $$(HOME)/Dev/vtk-5.10.1-install

INCLUDEPATH += $${VTKHOME}/include/vtk-5.10
LIBS += -L$${VTKHOME}/lib/vtk-5.10 -lvtkRendering -lvtkImaging -lvtkGraphics -lvtkFiltering \
       -lvtkGenericFiltering -lvtkHybrid -lvtkIO -lvtkftgl -lvtkVolumeRendering -lvtkNetCDF \
       -lvtkDICOMParser -lvtkCommon -lvtkexoIIc -lvtkexpat -lvtkfreetype -lvtkjpeg -lQVTK \
       -lvtkpng -lvtksys -lvtktiff -lvtkzlib -lm -lpthread -lvtkGeovis -lvtkViews  -lvtkInfoVis \
       -lvtkWidgets -lvtkGraphics -lvtkHybrid -lvtkImaging


SOURCES += main.cpp \
    mainwindow.cpp

HEADERS += \
    mainwindow.h

FORMS += \
    mainwindow.ui
