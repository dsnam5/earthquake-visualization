/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Wed Apr 24 23:27:52 2013
**      by: Qt User Interface Compiler version 4.8.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QStatusBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionOpen;
    QWidget *centralwidget;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout;
    QFrame *vtkFrame;
    QVBoxLayout *verticalLayout_3;
    QSpacerItem *verticalSpacer;
    QGridLayout *gridLayout;
    QPushButton *color_btn1_8;
    QLabel *color_lbl1_7;
    QPushButton *color_btn1_7;
    QLabel *color_lbl1_6;
    QPushButton *color_btn1_6;
    QLabel *color_lbl1_5;
    QPushButton *color_btn1_5;
    QLabel *color_lbl1_4;
    QPushButton *color_btn1_4;
    QLabel *color_lbl1_3;
    QPushButton *color_btn1_3;
    QLabel *color_lbl1_2;
    QPushButton *color_btn1_2;
    QLabel *color_lbl1;
    QPushButton *color_btn1;
    QLabel *color_lbl1_8;
    QSpacerItem *verticalSpacer_2;
    QVBoxLayout *verticalLayout;
    QLabel *zscaleslider_label;
    QSlider *zslider;
    QVBoxLayout *verticalLayout_2;
    QLabel *monthslider_label;
    QSlider *month_slider;
    QMenuBar *menubar;
    QMenu *menuFile;
    QStatusBar *statusbar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(845, 749);
        actionOpen = new QAction(MainWindow);
        actionOpen->setObjectName(QString::fromUtf8("actionOpen"));
        centralwidget = new QWidget(MainWindow);
        centralwidget->setObjectName(QString::fromUtf8("centralwidget"));
        verticalLayout_4 = new QVBoxLayout(centralwidget);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        vtkFrame = new QFrame(centralwidget);
        vtkFrame->setObjectName(QString::fromUtf8("vtkFrame"));
        vtkFrame->setFrameShape(QFrame::StyledPanel);
        vtkFrame->setFrameShadow(QFrame::Raised);

        horizontalLayout->addWidget(vtkFrame);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer);

        gridLayout = new QGridLayout();
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        color_btn1_8 = new QPushButton(centralwidget);
        color_btn1_8->setObjectName(QString::fromUtf8("color_btn1_8"));

        gridLayout->addWidget(color_btn1_8, 0, 1, 1, 1);

        color_lbl1_7 = new QLabel(centralwidget);
        color_lbl1_7->setObjectName(QString::fromUtf8("color_lbl1_7"));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(color_lbl1_7->sizePolicy().hasHeightForWidth());
        color_lbl1_7->setSizePolicy(sizePolicy);
        color_lbl1_7->setMinimumSize(QSize(20, 20));
        color_lbl1_7->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1_7, 1, 0, 1, 1);

        color_btn1_7 = new QPushButton(centralwidget);
        color_btn1_7->setObjectName(QString::fromUtf8("color_btn1_7"));

        gridLayout->addWidget(color_btn1_7, 1, 1, 1, 1);

        color_lbl1_6 = new QLabel(centralwidget);
        color_lbl1_6->setObjectName(QString::fromUtf8("color_lbl1_6"));
        QSizePolicy sizePolicy1(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(color_lbl1_6->sizePolicy().hasHeightForWidth());
        color_lbl1_6->setSizePolicy(sizePolicy1);
        color_lbl1_6->setMinimumSize(QSize(20, 20));
        color_lbl1_6->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1_6, 2, 0, 1, 1);

        color_btn1_6 = new QPushButton(centralwidget);
        color_btn1_6->setObjectName(QString::fromUtf8("color_btn1_6"));

        gridLayout->addWidget(color_btn1_6, 2, 1, 1, 1);

        color_lbl1_5 = new QLabel(centralwidget);
        color_lbl1_5->setObjectName(QString::fromUtf8("color_lbl1_5"));
        sizePolicy.setHeightForWidth(color_lbl1_5->sizePolicy().hasHeightForWidth());
        color_lbl1_5->setSizePolicy(sizePolicy);
        color_lbl1_5->setMinimumSize(QSize(20, 20));
        color_lbl1_5->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1_5, 3, 0, 1, 1);

        color_btn1_5 = new QPushButton(centralwidget);
        color_btn1_5->setObjectName(QString::fromUtf8("color_btn1_5"));

        gridLayout->addWidget(color_btn1_5, 3, 1, 1, 1);

        color_lbl1_4 = new QLabel(centralwidget);
        color_lbl1_4->setObjectName(QString::fromUtf8("color_lbl1_4"));
        sizePolicy.setHeightForWidth(color_lbl1_4->sizePolicy().hasHeightForWidth());
        color_lbl1_4->setSizePolicy(sizePolicy);
        color_lbl1_4->setMinimumSize(QSize(20, 20));
        color_lbl1_4->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1_4, 4, 0, 1, 1);

        color_btn1_4 = new QPushButton(centralwidget);
        color_btn1_4->setObjectName(QString::fromUtf8("color_btn1_4"));

        gridLayout->addWidget(color_btn1_4, 4, 1, 1, 1);

        color_lbl1_3 = new QLabel(centralwidget);
        color_lbl1_3->setObjectName(QString::fromUtf8("color_lbl1_3"));
        sizePolicy.setHeightForWidth(color_lbl1_3->sizePolicy().hasHeightForWidth());
        color_lbl1_3->setSizePolicy(sizePolicy);
        color_lbl1_3->setMinimumSize(QSize(20, 20));
        color_lbl1_3->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1_3, 5, 0, 1, 1);

        color_btn1_3 = new QPushButton(centralwidget);
        color_btn1_3->setObjectName(QString::fromUtf8("color_btn1_3"));

        gridLayout->addWidget(color_btn1_3, 5, 1, 1, 1);

        color_lbl1_2 = new QLabel(centralwidget);
        color_lbl1_2->setObjectName(QString::fromUtf8("color_lbl1_2"));
        sizePolicy.setHeightForWidth(color_lbl1_2->sizePolicy().hasHeightForWidth());
        color_lbl1_2->setSizePolicy(sizePolicy);
        color_lbl1_2->setMinimumSize(QSize(20, 20));
        color_lbl1_2->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1_2, 6, 0, 1, 1);

        color_btn1_2 = new QPushButton(centralwidget);
        color_btn1_2->setObjectName(QString::fromUtf8("color_btn1_2"));

        gridLayout->addWidget(color_btn1_2, 6, 1, 1, 1);

        color_lbl1 = new QLabel(centralwidget);
        color_lbl1->setObjectName(QString::fromUtf8("color_lbl1"));
        sizePolicy.setHeightForWidth(color_lbl1->sizePolicy().hasHeightForWidth());
        color_lbl1->setSizePolicy(sizePolicy);
        color_lbl1->setMinimumSize(QSize(20, 20));
        color_lbl1->setFrameShape(QFrame::Box);

        gridLayout->addWidget(color_lbl1, 7, 0, 1, 1);

        color_btn1 = new QPushButton(centralwidget);
        color_btn1->setObjectName(QString::fromUtf8("color_btn1"));

        gridLayout->addWidget(color_btn1, 7, 1, 1, 1);

        color_lbl1_8 = new QLabel(centralwidget);
        color_lbl1_8->setObjectName(QString::fromUtf8("color_lbl1_8"));
        sizePolicy.setHeightForWidth(color_lbl1_8->sizePolicy().hasHeightForWidth());
        color_lbl1_8->setSizePolicy(sizePolicy);
        color_lbl1_8->setMinimumSize(QSize(20, 20));
        color_lbl1_8->setFrameShape(QFrame::Box);
        color_lbl1_8->setFrameShadow(QFrame::Plain);

        gridLayout->addWidget(color_lbl1_8, 0, 0, 1, 1);

        gridLayout->setColumnStretch(0, 2);
        gridLayout->setColumnStretch(1, 1);

        verticalLayout_3->addLayout(gridLayout);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_3->addItem(verticalSpacer_2);


        horizontalLayout->addLayout(verticalLayout_3);

        horizontalLayout->setStretch(0, 4);
        horizontalLayout->setStretch(1, 1);

        verticalLayout_4->addLayout(horizontalLayout);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        zscaleslider_label = new QLabel(centralwidget);
        zscaleslider_label->setObjectName(QString::fromUtf8("zscaleslider_label"));
        zscaleslider_label->setAlignment(Qt::AlignCenter);

        verticalLayout->addWidget(zscaleslider_label);

        zslider = new QSlider(centralwidget);
        zslider->setObjectName(QString::fromUtf8("zslider"));
        zslider->setOrientation(Qt::Horizontal);

        verticalLayout->addWidget(zslider);


        verticalLayout_4->addLayout(verticalLayout);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        monthslider_label = new QLabel(centralwidget);
        monthslider_label->setObjectName(QString::fromUtf8("monthslider_label"));
        monthslider_label->setAlignment(Qt::AlignCenter);

        verticalLayout_2->addWidget(monthslider_label);

        month_slider = new QSlider(centralwidget);
        month_slider->setObjectName(QString::fromUtf8("month_slider"));
        month_slider->setOrientation(Qt::Horizontal);

        verticalLayout_2->addWidget(month_slider);


        verticalLayout_4->addLayout(verticalLayout_2);

        MainWindow->setCentralWidget(centralwidget);
        menubar = new QMenuBar(MainWindow);
        menubar->setObjectName(QString::fromUtf8("menubar"));
        menubar->setGeometry(QRect(0, 0, 845, 22));
        menuFile = new QMenu(menubar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        MainWindow->setMenuBar(menubar);
        statusbar = new QStatusBar(MainWindow);
        statusbar->setObjectName(QString::fromUtf8("statusbar"));
        MainWindow->setStatusBar(statusbar);

        menubar->addAction(menuFile->menuAction());
        menuFile->addAction(actionOpen);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionOpen->setText(QApplication::translate("MainWindow", "Open", 0, QApplication::UnicodeUTF8));
        color_btn1_8->setText(QApplication::translate("MainWindow", "color8", 0, QApplication::UnicodeUTF8));
        color_lbl1_7->setText(QString());
        color_btn1_7->setText(QApplication::translate("MainWindow", "color7", 0, QApplication::UnicodeUTF8));
        color_lbl1_6->setText(QString());
        color_btn1_6->setText(QApplication::translate("MainWindow", "color6", 0, QApplication::UnicodeUTF8));
        color_lbl1_5->setText(QString());
        color_btn1_5->setText(QApplication::translate("MainWindow", "color5", 0, QApplication::UnicodeUTF8));
        color_lbl1_4->setText(QString());
        color_btn1_4->setText(QApplication::translate("MainWindow", "color4", 0, QApplication::UnicodeUTF8));
        color_lbl1_3->setText(QString());
        color_btn1_3->setText(QApplication::translate("MainWindow", "color3", 0, QApplication::UnicodeUTF8));
        color_lbl1_2->setText(QString());
        color_btn1_2->setText(QApplication::translate("MainWindow", "color2", 0, QApplication::UnicodeUTF8));
        color_lbl1->setText(QString());
        color_btn1->setText(QApplication::translate("MainWindow", "color1", 0, QApplication::UnicodeUTF8));
        color_lbl1_8->setText(QString());
        zscaleslider_label->setText(QApplication::translate("MainWindow", "scale", 0, QApplication::UnicodeUTF8));
        monthslider_label->setText(QApplication::translate("MainWindow", "date", 0, QApplication::UnicodeUTF8));
        menuFile->setTitle(QApplication::translate("MainWindow", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
