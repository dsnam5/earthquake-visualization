#include <QApplication>
#include <QtCore>
#include <QtDebug>

#include "mainwindow.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    
    MainWindow mw;
    
    /*
    mw.setVisMode(MainWindow::Plane);
    double bounds[3];
    mw.createPlaneTexture(bounds);
    mw.createEarthquakeData("/Users/snam5/Dev/earthquake_vis/Data/earthquakesFULL.csv", bounds[0], bounds[1], bounds[2]);
    */
    
    mw.setVisMode(MainWindow::Sphere);
    mw.createSphereTexture("/Users/snam5/Dev/earthquake_vis/Data/2_no_clouds_8k_flipped.jpg");
    mw.createEarthquakeData("/Users/snam5/Dev/earthquake_vis/Data/earthquakesFULL.csv");
    
    
    mw.init_gui();
    
    mw.visualizeData2();
    
    mw.show();
    
    return a.exec();
}

//13 fields in a line
//DateTime,                       Latitude, Longitude, Depth,  Magnitude,   MagType,NbStations,Gap,Distance,RMS, Source, EventID,   Version
//2013-04-13T00:11:38.000+00:00,  62.966,  -149.080,  73.6,   1.4,          Ml,     24,        61, 0.2,     0.47, ak,    ak10694076,1366233108178
